@time using PyCall
@time using StringEncodings
@time using JSON
@time @pyimport serial as serial

ser = serial.Serial(
    port = "/dev/ttyACM1",
    baudrate = 115200
)

global jsonDict = ""

while true
    inData = ""

    ser[:write](encode("""{"code":1,"f_motors":{"forward":1,"backward":0},"s_motors":{"base":90,"forearm":90}}\n""", "UTF-8"))
    #print(encode("""{"code":1,"f_motors":{"forward":1,"backward":0},"s_motors":{"base":90,"forearm":90}}\n""", "UTF-8"))

    received = ""

    while received != "\n"
        received = ser[:read]()
        inData = string(inData, received)
    end

    transformedInData = replace(inData, "\n" => "")
    transformedInData = replace(inData, "\r" => "")
    println("test")
    try
        global jsonDict = JSON.parse(transformedInData)
        print(jsonDict["code"])
    catch err
        println("Error parsing")
    finally
        ser[:flush]()
    end

    sleep(0.2)
end

#for calibration

